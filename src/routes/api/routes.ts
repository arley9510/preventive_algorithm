import * as Router from 'koa-router';

// Local controllers
import { routerSystem } from '../public/routes';
import controller = require('../../controllers');

const routerApi = new Router();

const initialPath = '/api/v1/';

export { routerApi };
