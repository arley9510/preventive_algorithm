import * as Router from 'koa-router';

// Local controllers
import controller = require('../../controllers');

const routerSystem = new Router();

const initialPath = '/api/public/';

// Public health check
routerSystem.get(`${initialPath}health`, controller.Site.healthCheck);

export { routerSystem };
