// Router libraries
import * as CombineRouter from 'koa-combine-routers';

// Local routes
import { routerApi } from './api/routes';
import { routerSystem } from './public/routes';

const router = CombineRouter(
    routerApi,
    routerSystem,
);

export { router };
