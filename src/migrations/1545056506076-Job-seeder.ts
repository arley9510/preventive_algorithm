import {MigrationInterface, QueryRunner} from "typeorm";

export class JobSeeder1545056506076 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`INSERT INTO public.job(name, label, active)
                                 VALUES
                                 ('change_oil', 'Cambiar aceite motor', true),
                                 ('replace_front_tires', 'Reemplazar llantas delanteras', true),
                                 ('change_clutch_kit', 'Cambiar kit embrague', true),
                                 ('change_distribution_kit', 'Cambiar kit repartición', true),
                                 ('alternator_repair', 'Reparación alternador', true),
                                 ('alignment', 'Alineación', true),
                                 ('gas_point_setting', 'Puesta punto gas', true)`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM public.job WHERE
                                        name = 'change_oil' or
                                        name = 'replace_front_tires' or
                                        name = 'change_clutch_kit' or
                                        name = 'change_distribution_kit' or
                                        name = 'alternator_repair' or
                                        name = 'alignment' or
                                        name = 'gas_point_setting'`);
    }

}
