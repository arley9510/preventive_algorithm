import {MigrationInterface, QueryRunner} from "typeorm";

export class PreventiveDurationTypeSeeder1545056497390 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`INSERT INTO public.preventive_duration_type(name, label, active)
                                 VALUES
                                 ('distance', 'Distancia', true),
                                 ('days', 'Dias', true)`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM public.preventive_duration_type WHERE
                                        name = 'distance' or
                                        name = 'days'`);
    }

}
