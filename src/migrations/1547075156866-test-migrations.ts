import { MigrationInterface, QueryRunner } from "typeorm";

export class testMigrations1547075156866 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`insert into owner_contract_status (name, label, active)
                                 values ('active', 'Activo', true),
                                        ('inactive', 'Inactivo', true)
        `);

        await queryRunner.query(`insert into driver_assigned (driver_id, date_assignment, created_at, observation,
                                                              odometer, vehicle_id)
                                 values ('1', '2018-01-01', now(), 'Conductor Asignado 1', '50000', '1'),
                                        ('2', '2018-01-01', now(), 'Conductor Asignado 2', '60000', '2'),
                                        ('3', '2018-01-01', now(), 'Conductor Asignado 3', '70000', '3'),
                                        ('4', '2018-01-01', now(), 'Conductor Asignado 4', '80000', '4'),
                                        ('5', '2018-01-01', now(), 'Conductor Asignado 5', '90000', '5'),
                                        ('6', '2018-01-01', now(), 'Conductor Asignado 6', '100000', '6'),
                                        ('7', '2018-01-01', now(), 'Conductor Asignado 7', '110000', '7'),
                                        ('8', '2018-01-01', now(), 'Conductor Asignado 8', '120000', '8'),
                                        ('9', '2018-01-01', now(), 'Conductor Asignado 9', '130000', '9'),
                                        ('10', '2018-01-01', now(), 'Conductor Asignado 10', '140000', '10')
        `);

        await queryRunner.query(`insert into owner_contract (number, date_start, date_end, observation, owner_id,
                                                             created_at, total_payment, installments,
                                                             owner_contract_status_id, vehicle_id, interest_rate)
                                 values ('Contrato 1', '2018-01-01', '2022-01-01', 'Contrato 1', 1, now(), '30000000',
                                         '48', 1, 1, '1.5'),
                                        ('Contrato 2', '2018-01-01', '2022-01-01', 'Contrato 2', 2, now(), '31000000',
                                         '48', 1, 2, '1.5'),
                                        ('Contrato 3', '2018-01-01', '2022-01-01', 'Contrato 3', 3, now(), '32000000',
                                         '48', 1, 3, '1.5'),
                                        ('Contrato 4', '2018-01-01', '2022-01-01', 'Contrato 4', 4, now(), '33000000',
                                         '48', 1, 4, '1.5'),
                                        ('Contrato 5', '2018-01-01', '2022-01-01', 'Contrato 5', 5, now(), '34000000',
                                         '48', 1, 5, '1.5'),
                                        ('Contrato 6', '2018-01-01', '2022-01-01', 'Contrato 6', 6, now(), '35000000',
                                         '48', 1, 6, '1.5'),
                                        ('Contrato 7', '2018-01-01', '2022-01-01', 'Contrato 7', 7, now(), '36000000',
                                         '48', 1, 7, '1.5'),
                                        ('Contrato 8', '2018-01-01', '2022-01-01', 'Contrato 8', 8, now(), '37000000',
                                         '48', 1, 8, '1.5'),
                                        ('Contrato 9', '2018-01-01', '2022-01-01', 'Contrato 9', 9, now(), '38000000',
                                         '48', 1, 9, '1.5'),
                                        ('Contrato 10', '2018-01-01', '2022-01-01', 'Contrato 10', 10, now(),
                                         '39000000', '48', 1, 10, '1.5')
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM owner_contract_status WHERE id >= 1`);
        await queryRunner.query(`DELETE FROM driver_assigned WHERE id >= 1`);
        await queryRunner.query(`DELETE FROM owner_contract WHERE id >= 1`);
    }

}
