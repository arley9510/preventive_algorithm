import { MigrationInterface, QueryRunner } from 'typeorm';

export class vehicleTableSeeder1546987652993 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`INSERT INTO public.vehicle(plate, year, engine_number, chasis_number, is_new, created_by, created_at, city_id, enterprise_id, vehicle_status_id, vehicle_model_id, vehicle_fuel_type_id, property_card)
          VALUES ('WCM248', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 11, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WEX356', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 11, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WHQ202', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 11, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WLT386', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 11, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WLU880', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 11, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WML945', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 12, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WML951', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 12, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WMM772', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 12, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('VEA601', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 12, NOW(), 1, 1, 1, 1, 1, '677863343'),
                 ('WNT130', 2018, 'LSGSA51M4GY011667', 'K7MA812UB76534', TRUE, 12, NOW(), 1, 1, 1, 1, 1, '677863343')
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE
                                 FROM public.vehicle
                                 WHERE plate like '%WCM248%'
                                 OR plate like '%WEX356%'
                                 OR plate like '%WHQ202%'
                                 OR plate like '%WLT386%'
                                 OR plate like '%WLU880%'
                                 OR plate like '%WML945%'
                                 OR plate like '%WML951%'
                                 OR plate like '%WMM772%'
                                 OR plate like '%VEA601%'
                                 OR plate like '%WNT130%'`);
    }

}
