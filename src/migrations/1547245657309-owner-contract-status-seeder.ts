import {MigrationInterface, QueryRunner} from "typeorm";

export class ownerContractStatusSeeder1547245657309 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('insert into owner_contract_status (name, label, active)\n' +
            'values\n' +
            '(\'active\',\'Activo\',true),\n' +
            '(\'inactive\', \'Inactivo\', true)');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`delete from owner_contract_status`);
    }

}
