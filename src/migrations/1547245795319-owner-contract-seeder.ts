import {MigrationInterface, QueryRunner} from "typeorm";

export class ownerContractSeeder1547245795319 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('' +
            'insert into owner_contract (number, date_start, date_end, observation, owner_id, created_at, total_payment, installments, owner_contract_status_id, vehicle_id, interest_rate) values \n' +
            '(\'Contrato 1\', \'2018-01-01\', \'2022-01-01\', \'Contrato 1\', 1, now(), \'30000000\', \'48\', 1, 1, \'1.5\'),\n' +
            '(\'Contrato 2\', \'2018-01-01\', \'2022-01-01\', \'Contrato 2\', 2, now(), \'31000000\', \'48\', 1, 2, \'1.5\'),\n' +
            '(\'Contrato 3\', \'2018-01-01\', \'2022-01-01\', \'Contrato 3\', 3, now(), \'32000000\', \'48\', 1, 3, \'1.5\'),\n' +
            '(\'Contrato 4\', \'2018-01-01\', \'2022-01-01\', \'Contrato 4\', 4, now(), \'33000000\', \'48\', 1, 4, \'1.5\'),\n' +
            '(\'Contrato 5\', \'2018-01-01\', \'2022-01-01\', \'Contrato 5\', 5, now(), \'34000000\', \'48\', 1, 5, \'1.5\'),\n' +
            '(\'Contrato 6\', \'2018-01-01\', \'2022-01-01\', \'Contrato 6\', 6, now(), \'35000000\', \'48\', 1, 6, \'1.5\'),\n' +
            '(\'Contrato 7\', \'2018-01-01\', \'2022-01-01\', \'Contrato 7\', 7, now(), \'36000000\', \'48\', 1, 7, \'1.5\'),\n' +
            '(\'Contrato 8\', \'2018-01-01\', \'2022-01-01\', \'Contrato 8\', 8, now(), \'37000000\', \'48\', 1, 8, \'1.5\'),\n' +
            '(\'Contrato 9\', \'2018-01-01\', \'2022-01-01\', \'Contrato 9\', 9, now(), \'38000000\', \'48\', 1, 9, \'1.5\'),\n' +
            '(\'Contrato 10\', \'2018-01-01\', \'2022-01-01\', \'Contrato 10\', 10, now(), \'39000000\', \'48\', 1, 10, \'1.5\');');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`delete from owner_contract`);
    }

}
