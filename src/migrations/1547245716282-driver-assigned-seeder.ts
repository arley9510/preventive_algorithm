import {MigrationInterface, QueryRunner} from "typeorm";

export class driverAssignedSeeder1547245716282 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('' +
            'insert into driver_assigned (driver_id, date_assignment, created_at, observation, odometer, vehicle_id) values \n' +
            '(\'1\', \'2018-01-01\', now(), \'Conductor Asignado 1\', \'50000\', \'1\'),\n' +
            '(\'2\', \'2018-01-01\', now(), \'Conductor Asignado 2\', \'60000\', \'2\'),\n' +
            '(\'3\', \'2018-01-01\', now(), \'Conductor Asignado 3\', \'70000\', \'3\'),\n' +
            '(\'4\', \'2018-01-01\', now(), \'Conductor Asignado 4\', \'80000\', \'4\'),\n' +
            '(\'5\', \'2018-01-01\', now(), \'Conductor Asignado 5\', \'90000\', \'5\'),\n' +
            '(\'6\', \'2018-01-01\', now(), \'Conductor Asignado 6\', \'100000\', \'6\'),\n' +
            '(\'7\', \'2018-01-01\', now(), \'Conductor Asignado 7\', \'110000\', \'7\'),\n' +
            '(\'8\', \'2018-01-01\', now(), \'Conductor Asignado 8\', \'120000\', \'8\'),\n' +
            '(\'9\', \'2018-01-01\', now(), \'Conductor Asignado 9\', \'130000\', \'9\'),\n' +
            '(\'10\', \'2018-01-01\', now(), \'Conductor Asignado 10\', \'140000\', \'10\')');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`delete from driver_assigned`);
    }

}
