import {MigrationInterface, QueryRunner} from "typeorm";

export class vehicleModelSeeder1542159818030 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.query(
            `INSERT INTO "vehicle_model"(name, label, active, vehicle_brand_id)` +
            `VALUES` +
            `('724', '724', true, (select id from vehicle_brand where name = 'chevrolet')), ` +
            `('5.5', '5.5', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('a_class', 'A-Class', true, (select id from vehicle_brand where name = 'jac')), ` +
            `('accent', 'Accent', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('accent_gl', 'Accent GL', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('accent_verna_gls', 'Accent verna GLS', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('actyon', 'Actyon', true, (select id from vehicle_brand where name = 'ssangyong')), ` +
            `('alto', 'Alto', true, (select id from vehicle_brand where name = 'suzuki')), ` +
            `('atos', 'Atos', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('aveo', 'Aveo', true, (select id from vehicle_brand where name = 'chevrolet')), ` +
            `('c37_minibus', 'C37 MINIBUS', true, (select id from vehicle_brand where name = 'dfsk')), ` +
            `('cerato', 'Cerato', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('clio_express', 'Clio Express', true, (select id from vehicle_brand where name = 'renault')), ` +
            `('corolla', 'Corolla', true, (select id from vehicle_brand where name = 'toyota')), ` +
            `('corsa', 'Corsa', true, (select id from vehicle_brand where name = 'chevrolet')), ` +
            `('cs35', 'CS35', true, (select id from vehicle_brand where name = 'changan')), ` +
            `('duster', 'Duster', true, (select id from vehicle_brand where name = 'renault')), ` +
            `('e6', 'E6', true, (select id from vehicle_brand where name = 'byd')), ` +
            `('ecosport', 'Ecosport', true, (select id from vehicle_brand where name = 'ford')), ` +
            `('ekotaxi', 'Ekotaxi', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('elantra', 'Elantra', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('f3', 'F3', true, (select id from vehicle_brand where name = 'byd')), ` +
            `('fulwin', 'Fulwin', true, (select id from vehicle_brand where name = 'chery')), ` +
            `('glory', 'Glory', true, (select id from vehicle_brand where name = 'dfsk')), ` +
            `('gol', 'Gol', true, (select id from vehicle_brand where name = 'volkswagen')), ` +
            `('gol_sw', 'Gol SW', true, (select id from vehicle_brand where name = 'volkswagen')), ` +
            `('grand_i10', 'Grand i10', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('h230', 'H230', true, (select id from vehicle_brand where name = 'brilliance')), ` +
            `('haval_h5', 'Haval H5', true, (select id from vehicle_brand where name = 'great_wall')), ` +
            `('hunter', 'Hunter', true, (select id from vehicle_brand where name = 'zotye')), ` +
            `('i10', 'i10', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('j6', 'J6', true, (select id from vehicle_brand where name = 'jac')), ` +
            `('k07_minivan', 'K07 MINIVAN', true, (select id from vehicle_brand where name = 'dfsk')), ` +
            `('korando', 'Korando', true, (select id from vehicle_brand where name = 'ssangyong')), ` +
            `('logan', 'Logan', true, (select id from vehicle_brand where name = 'renault')), ` +
            `('midi', 'Midi', true, (select id from vehicle_brand where name = 'foton')), ` +
            `('nomad', 'Nomad', true, (select id from vehicle_brand where name = 'zotye')), ` +
            `('palio', 'Palio', true, (select id from vehicle_brand where name = 'fiat')), ` +
            `('picanto_ion', 'Picanto Ion', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('picanto_morning', 'Picanto Morning', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('priora', 'Priora', true, (select id from vehicle_brand where name = 'lada')), ` +
            `('rio', 'Rio', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('rio_ls', 'Rio LS', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('sail', 'Sail', true, (select id from vehicle_brand where name = 'chevrolet')), ` +
            `('sentra', 'Sentra', true, (select id from vehicle_brand where name = 'nissan')), ` +
            `('sephia', 'Sephia', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('siena', 'Siena', true, (select id from vehicle_brand where name = 'fiat')), ` +
            `('soul', 'Soul', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('spark', 'Spark', true, (select id from vehicle_brand where name = 'chevrolet')), ` +
            `('sportage', 'Sportage', true, (select id from vehicle_brand where name = 'kia')), ` +
            `('star_jac', 'Star Jac', true, (select id from vehicle_brand where name = 'jac')), ` +
            `('street', 'Street', true, (select id from vehicle_brand where name = 'jac')), ` +
            `('sy7', 'SY7', true, (select id from vehicle_brand where name = 'brilliance')), ` +
            `('taxi_power', 'Taxi Power', true, (select id from vehicle_brand where name = 'chevrolet')), ` +
            `('terios', 'Terios', true, (select id from vehicle_brand where name = 'daihatsu')), ` +
            `('tiggo', 'Tiggo', true, (select id from vehicle_brand where name = 'chery')), ` +
            `('tiida', 'Tiida', true, (select id from vehicle_brand where name = 'nissan')), ` +
            `('tracker', 'Tracker', true, (select id from vehicle_brand where name = 'chevrolet')), ` +
            `('uno_vivace', 'Uno Vivace', true, (select id from vehicle_brand where name = 'fiat')), ` +
            `('vision', 'Vision', true, (select id from vehicle_brand where name = 'hyundai')), ` +
            `('volex', 'Volex', true, (select id from vehicle_brand where name = 'great_wall')), ` +
            `('voyage', 'Voyage', true, (select id from vehicle_brand where name = 'volkswagen')), ` +
            `('yaris', 'Yaris', true, (select id from vehicle_brand where name = 'toyota')), ` +
            `('zotye', 'Zotye', true, (select id from vehicle_brand where name = 'zotye'))`


        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM "vehicle_model" WHERE ` +
            `name ="724" or ` +
            `name ="5.5" or ` +
            `name ="a_class" or ` +
            `name ="accent" or ` +
            `name ="accent_gl" or ` +
            `name ="accent_verna_gls" or ` +
            `name ="actyon" or ` +
            `name ="alto" or ` +
            `name ="atos" or ` +
            `name ="aveo" or ` +
            `name ="c37_minibus" or ` +
            `name ="cerato" or ` +
            `name ="clio_express" or ` +
            `name ="corolla" or ` +
            `name ="corsa" or ` +
            `name ="cs35" or ` +
            `name ="duster" or ` +
            `name ="e6" or ` +
            `name ="ecosport" or ` +
            `name ="ekotaxi" or ` +
            `name ="elantra" or ` +
            `name ="f3" or ` +
            `name ="fulwin" or ` +
            `name ="glory" or ` +
            `name ="gol" or ` +
            `name ="gol_sw" or ` +
            `name ="grand_i10" or ` +
            `name ="h230" or ` +
            `name ="haval_h5" or ` +
            `name ="hunter" or ` +
            `name ="i10" or ` +
            `name ="j6" or ` +
            `name ="k07_minivan" or ` +
            `name ="korando" or ` +
            `name ="logan" or ` +
            `name ="midi" or ` +
            `name ="nomad" or ` +
            `name ="palio" or ` +
            `name ="picanto_ion" or ` +
            `name ="picanto_morning" or ` +
            `name ="priora" or ` +
            `name ="rio" or ` +
            `name ="rio_ls" or ` +
            `name ="sail" or ` +
            `name ="sentra" or ` +
            `name ="sephia" or ` +
            `name ="siena" or ` +
            `name ="soul" or ` +
            `name ="spark" or ` +
            `name ="sportage" or ` +
            `name ="star_jac" or ` +
            `name ="street" or ` +
            `name ="sy7" or ` +
            `name ="taxi_power" or ` +
            `name ="terios" or ` +
            `name ="tiggo" or ` +
            `name ="tiida" or ` +
            `name ="tracker" or ` +
            `name ="uno_vivace" or ` +
            `name ="vision" or ` +
            `name ="volex" or ` +
            `name ="voyage" or ` +
            `name ="yaris" or ` +
            `name ="zotye"`
        );
    }

}
