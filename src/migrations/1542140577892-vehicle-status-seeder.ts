import {MigrationInterface, QueryRunner} from "typeorm";

export class vehicleStatusSeeder1542140577892 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.query(
            `INSERT INTO "vehicle_status"(name, label, active)` +
            `VALUES` +
            `('active', 'Activo', true),` +
            `('inactive', 'Inactivo', true)`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM "vehicle_status" WHERE ` +
            `name = "active"              or ` +
            `name = "inactive"            or `
        );
    }

}
