import {MigrationInterface, QueryRunner} from "typeorm";

export class vehicleBrandSeeder1542159620651 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.query(
            `INSERT INTO "vehicle_brand"(name, label, active)` +
            `VALUES` +
            `('brilliance', 'Brilliance', true), ` +
            `('byd', 'BYD', true), ` +
            `('changan', 'CHANGAN', true), ` +
            `('chery', 'Chery', true), ` +
            `('chevrolet', 'Chevrolet', true), ` +
            `('daihatsu', 'Daihatsu', true), ` +
            `('dfsk', 'DFSK', true), ` +
            `('fiat', 'Fiat', true), ` +
            `('ford', 'Ford', true), ` +
            `('foton', 'Foton', true), ` +
            `('great_wall', 'Great Wall', true), ` +
            `('hyundai', 'Hyundai', true), ` +
            `('jac', 'JAC', true), ` +
            `('kia', 'Kia', true), ` +
            `('lada', 'Lada', true), ` +
            `('nissan', 'Nissan', true), ` +
            `('renault', 'Renault', true), ` +
            `('ssangyong', 'SsangYong', true), ` +
            `('suzuki', 'Suzuki', true), ` +
            `('toyota', 'Toyota', true), ` +
            `('volkswagen', 'Volkswagen', true), ` +
            `('zotye', 'Zotye', true)`

        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM "vehicle_brand" WHERE ` +
            `name ="brilliance" or ` +
            `name ="byd" or ` +
            `name ="changan" or ` +
            `name ="chery" or ` +
            `name ="chevrolet" or ` +
            `name ="daihatsu" or ` +
            `name ="dfsk" or ` +
            `name ="fiat" or ` +
            `name ="ford" or ` +
            `name ="foton" or ` +
            `name ="great_wall" or ` +
            `name ="hyundai" or ` +
            `name ="jac" or ` +
            `name ="kia" or ` +
            `name ="lada" or ` +
            `name ="nissan" or ` +
            `name ="renault" or ` +
            `name ="ssangyong" or ` +
            `name ="suzuki" or ` +
            `name ="toyota" or ` +
            `name ="volkswagen" or ` +
            `name ="zotye"`

        );
    }

}
