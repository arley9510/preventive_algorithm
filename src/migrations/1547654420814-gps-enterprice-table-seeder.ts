import { MigrationInterface, QueryRunner } from "typeorm";

export class gpsEnterpriceTableSeeder1547654420814 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`
          INSERT INTO gps_enterprise(name, label)
          VALUES ('widetech', 'Widetech'),
                 ('acceso', 'Acceso')
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`
          DELETE
          FROM gps_enterprise
          WHERE name IN ('widetech', 'acceso')
        `);
    }

}
