import { MigrationInterface, QueryRunner } from "typeorm";

export class vehicleEnterpriseTableSeeder1547654456490 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        for (let i = 1; i < 11; i++) {
            await queryRunner.query(`
              INSERT INTO public.vehicle_enterprise (vehicle_id, enterprise_id, gps_enterprise, active)
              VALUES (${i}, 1, 1, true)
            `);
        }
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE
                                 FROM taximo.public.vehicle_enterprise
                                 WHERE vehicle_id IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
        `);
    }

}
