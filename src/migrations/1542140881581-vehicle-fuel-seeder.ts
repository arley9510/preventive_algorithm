import {MigrationInterface, QueryRunner} from "typeorm";

export class vehicleFuelSeeder1542140881581 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {

        await queryRunner.query(
            `INSERT INTO "vehicle_fuel_type"(name, label, active)` +
            `VALUES` +
            `('gas', 'Gas', true),` +
            `('gasoline', 'Gasolina', true)`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM "vehicle_fuel_type" WHERE ` +
            `name = "gas"              or ` +
            `name = "gasoline"         or `
        );
    }

}
