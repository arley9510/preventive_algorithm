import * as dotenv from 'dotenv';

import { IConfig } from './interfaces/IconfigInterface';

dotenv.config();

const db = `posgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB}`;

const config: IConfig = {
    port: parseInt(process.env.PORT),
    debugLogging: process.env.NODE_ENV === 'development',
    dbsslconn: process.env.NODE_ENV !== 'development',
    databaseUrl: db
};

export { config };
