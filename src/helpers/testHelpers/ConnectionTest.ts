import * as dotenv from 'dotenv';
import { createConnection } from 'typeorm';

import { dbConfig } from '../../ConnectionConfig';

dotenv.config();

export class ConnectionTest {
    public static getConnection() {
        if (process.env.NODE_ENV === 'development') {
            return createConnection(dbConfig);
        }
    }
}
