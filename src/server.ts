import 'reflect-metadata';
import * as Koa from 'koa';
import * as jwt from 'koa-jwt';
import * as dotenv from 'dotenv';
import * as cors from '@koa/cors';
import * as winston from 'winston';
import * as helmet from 'koa-helmet';
import * as Sentry from '@sentry/node';
import { createConnection } from 'typeorm';
import * as bodyParser from 'koa-bodyparser';
import { paginate } from '@panderalabs/koa-pageable';

import { config } from './config';
import { logger } from './logging';
import { router } from './routes/routes';
import { dbConfig } from './ConnectionConfig';

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config();

// note that its not active database connection
// TypeORM creates you connection pull to uses connections from pull on your requests
createConnection(dbConfig).then(async () => {

    const app = new Koa();

    // Sentry client provisioning
    Sentry.init({dsn: process.env.SENTRY_DNS});

    // Provides important security headers to make your app more secure
    app.use(helmet());

    // Enable cors with default options
    app.use(cors());

    // Logger middleware -> use winston as logger (logging.ts with config)
    app.use(logger(winston));

    // Enable bodyParser with default options
    app.use(bodyParser());

    // Enable JWT protection for the routes
    app.use(jwt({ secret: process.env.SECRET_KEY }).unless({ path: [new RegExp('/api/public/', 'i')] }));

    // Enable pagination
    app.use(paginate);

    // this provides the routes
    app.use(router());

    // start the app
    app.listen(config.port);

    console.log(`Server running on port ${config.port}`);

}).catch(error => console.log('TypeORM connection error: ', error));
