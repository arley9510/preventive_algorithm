import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { Part } from './Part';
import { PreventiveDurationType } from './PreventiveDurationType';
import { PreventiveJobScheduled } from './PreventiveJobScheduled';
import { PreventivePartScheduled } from './PreventivePartScheduled';


@Entity('preventive_part')
export class PreventivePart {

    @PrimaryGeneratedColumn({
        name: 'id'
        })
    Id: number;


    @Column('integer', {
        nullable: false,
        name: 'green_alert'
        })
    GreenAlert: number;


    @Column('integer', {
        nullable: false,
        name: 'orange_alert'
        })
    OrangeAlert: number;


    @Column('integer', {
        nullable: false,
        name: 'red_alert'
        })
    RedAlert: number;


    @Column('boolean', {
        nullable: false,
        name: 'active'
        })
    Active: boolean;



    @ManyToOne(type => Part, Part => Part.PreventiveParts, {  nullable: false, })
    @JoinColumn({ name: 'part_id'})
    Part: Part | null;



    @ManyToOne(type => PreventiveDurationType, PreventiveDurationType => PreventiveDurationType.PreventiveParts, {  nullable: false, })
    @JoinColumn({ name: 'preventive_duration_type_id'})
    PreventiveDurationType: PreventiveDurationType | null;

    @OneToMany(type => PreventivePartScheduled, PreventivePartScheduled => PreventivePartScheduled.PreventivePart)
    PreventivePartScheduled: PreventivePartScheduled[];

    @Column('integer', {
        nullable: false,
        name: 'city_id'
        })
    CityId: number;

}
