import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { Part } from './Part';
import { PreventiveJobScheduled } from './PreventiveJobScheduled';
import { PreventiveJob } from './PreventiveJob';
import { PreventivePart } from './PreventivePart';
import { PreventiveJobReported } from './PreventiveJobReported';
import { PreventivePartReported } from './PreventivePartReported';

@Entity('preventive_part_scheduled')
export class PreventivePartScheduled {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;


    @Column('integer', {
        nullable: false,
        name: 'city_id'
    })
    CityId: number;


    @Column('integer', {
        nullable: false,
        name: 'vehicle_id'
    })
    VehicleId: number;

    @Column('integer', {
        nullable: false,
        name: 'value'
    })
    Value: number;


    @Column('date', {
        nullable: false,
        name: 'date_scheduled'
    })
    DateScheduled: Date;

    @ManyToOne(type => Part, Part => Part.PreventivePartScheduled, {  nullable: false, })
    @JoinColumn({ name: 'part_id'})
    Part: Part | null;

    @ManyToOne(type => PreventivePart, PreventivePart => PreventivePart.PreventivePartScheduled, {  nullable: false, })
    @JoinColumn({ name: 'preventive_part_id'})
    PreventivePart: PreventivePart | null;

    @OneToMany(type => PreventivePartReported, PreventivePartReported => PreventivePartReported.PreventivePartScheduled)
    PreventivePartReported: PreventivePartReported[];

}
