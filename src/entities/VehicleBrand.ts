import {Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {VehicleModel} from "./VehicleModel";


@Entity("vehicle_brand")
export class VehicleBrand {

    @PrimaryGeneratedColumn({ 
        name:"id"
        })
    Id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"name"
        })
    Name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"label"
        })
    Label:string;
        

    @Column("boolean",{ 
        nullable:false,
        name:"active"
        })
    Active:boolean;
        

   
    @OneToMany(type=>VehicleModel, VehicleModel=>VehicleModel.VehicleBrand)
    VehicleModels:VehicleModel[];
    
}
