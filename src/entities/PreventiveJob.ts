import {Index,Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {Job} from "./Job";
import {PreventiveDurationType} from "./PreventiveDurationType";
import { PreventiveJobScheduled } from './PreventiveJobScheduled';


@Entity("preventive_job")
export class PreventiveJob {

    @PrimaryGeneratedColumn({ 
        name:"id"
        })
    Id:number;
        

    @Column("integer",{ 
        nullable:false,
        name:"green_alert"
        })
    GreenAlert:number;
        

    @Column("integer",{ 
        nullable:false,
        name:"orange_alert"
        })
    OrangeAlert:number;
        

    @Column("integer",{ 
        nullable:false,
        name:"red_alert"
        })
    RedAlert:number;
        

    @Column("boolean",{ 
        nullable:false,
        name:"active"
        })
    Active:boolean;
        

   
    @ManyToOne(type=>Job, Job=>Job.PreventiveJobs,{  nullable:false, })
    @JoinColumn({ name:'job_id'})
    Job:Job | null;


   
    @ManyToOne(type=>PreventiveDurationType, PreventiveDurationType=>PreventiveDurationType.PreventiveJobs,{  nullable:false, })
    @JoinColumn({ name:'preventive_duration_type_id'})
    PreventiveDurationType:PreventiveDurationType | null;

    @OneToMany(type => PreventiveJobScheduled, PreventiveJobScheduled => PreventiveJobScheduled.PreventiveJob)
    PreventiveJobScheduled: PreventiveJobScheduled[];

    @Column("integer",{ 
        nullable:false,
        name:"city_id"
        })
    CityId:number;
        
}
