import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { Job } from './Job';
import { PreventiveJobReported } from './PreventiveJobReported';
import { PreventiveDurationType } from './PreventiveDurationType';
import { PreventiveJob } from './PreventiveJob';

@Entity('preventive_job_scheduled')
export class PreventiveJobScheduled {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;


    @Column('integer', {
        nullable: false,
        name: 'city_id'
    })
    CityId: number;


    @Column('integer', {
        nullable: false,
        name: 'vehicle_id'
    })
    VehicleId: number;

    @Column('integer', {
        nullable: false,
        name: 'value'
    })
    Value: number;


    @Column('date', {
        nullable: false,
        name: 'date_scheduled'
    })
    DateScheduled: Date;

    @ManyToOne(type => Job, Job => Job.PreventiveJobScheduled, {  nullable: false, })
    @JoinColumn({ name: 'job_id'})
    Job: Job | null;

    @ManyToOne(type => PreventiveJob, PreventiveJob => PreventiveJob.PreventiveJobScheduled, {  nullable: false, })
    @JoinColumn({ name: 'preventive_job_id'})
    PreventiveJob: PreventiveJob | null;

    @OneToMany(type => PreventiveJobReported, PreventiveJobReported => PreventiveJobReported.PreventiveJobScheduled)
    PreventiveJobReported: PreventiveJobReported[];
}
