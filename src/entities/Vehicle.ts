import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    JoinColumn, OneToMany,
} from 'typeorm';
import { VehicleStatus } from './VehicleStatus';
import { VehicleModel } from './VehicleModel';
import { VehicleFuelType } from './VehicleFuelType';
import { DriverAssigned } from './DriverAssigned';
import { OwnerContract } from './OwnerContract';

@Entity('vehicle')
export class Vehicle {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;

    @Column('character varying', {
        nullable: false,
        length: 10,
        name: 'plate'
    })
    Plate: string;

    @Column('character varying', {
        nullable: true,
        length: 255,
        name: 'property_card'
    })
    PropertyCard: string;

    @Column('integer', {
        nullable: false,
        name: 'year'
    })
    Year: number;

    @Column('character varying', {
        nullable: false,
        length: 256,
        name: 'engine_number'
    })
    EngineNumber: string;

    @Column('character varying', {
        nullable: false,
        length: 256,
        name: 'chasis_number'
    })
    ChasisNumber: string;

    @Column('boolean', {
        nullable: false,
        name: 'is_new'
    })
    IsNew: boolean;

    @Column('integer', {
        nullable: false,
        name: 'created_by'
    })
    CreatedBy: number;

    @Column('timestamp without time zone', {
        nullable: false,
        default: 'now()',
        name: 'created_at'
    })
    CreatedAt: Date;

    @ManyToOne(() => VehicleStatus, VehicleStatus => VehicleStatus.Vehicles, {nullable: false})
    @JoinColumn({name: 'vehicle_status_id'})
    VehicleStatus: VehicleStatus | null;

    @ManyToOne(() => VehicleModel, VehicleModel => VehicleModel.Vehicles, {nullable: false})
    @JoinColumn({name: 'vehicle_model_id'})
    VehicleModel: VehicleModel | null;

    @Column('integer', {
        nullable: false,
        name: 'city_id'
    })
    CityId: number;

    @ManyToOne(() => VehicleFuelType, VehicleFuelType => VehicleFuelType.Vehicles, {nullable: false})
    @JoinColumn({name: 'vehicle_fuel_type_id'})
    VehicleFuelType: VehicleFuelType | null;

   
    @OneToMany(type=>DriverAssigned, DriverAssigned=>DriverAssigned.Vehicle)
    DriverAssigneds:DriverAssigned[];
    

    @Column('integer', {
        name: 'enterprise_id',
        nullable: false
    })
    EnterpriseId: number;

    @OneToMany(type=>OwnerContract, OwnerContract=>OwnerContract.Vehicle)
    OwnerContracts:OwnerContract[];

}
