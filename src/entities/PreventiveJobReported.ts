import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { Job } from './Job';
import { PreventiveJobScheduled } from './PreventiveJobScheduled';
import { PreventiveJob } from './PreventiveJob';


@Entity('preventive_job_reported')
export class PreventiveJobReported {

    @PrimaryGeneratedColumn({
        name: 'id'
        })
    Id: number;


    @Column('date', {
        nullable: false,
        name: 'date'
        })
    Date: string;


    @Column('integer', {
        nullable: false,
        name: 'odometer'
        })
    Odometer: number;

    @Column('timestamp without time zone', {
        nullable: false,
        default: 'now()',
        name: 'created_at'
        })
    CreatedAt: Date;

    @Column('character varying', {
        nullable: false,
        length: 50,
        name: 'type'
    })
    Type: string;

    @ManyToOne(type => Job, Job => Job.PreventiveJobReporteds, {  nullable: false, })
    @JoinColumn({ name: 'job_id'})
    Job: Job | null;

    @ManyToOne(type => PreventiveJobScheduled, PreventiveJobScheduled => PreventiveJobScheduled.PreventiveJobReported, {  nullable: false, })
    @JoinColumn({ name: 'preventive_job_scheduled_id'})
    PreventiveJobScheduled: PreventiveJobScheduled | null;

}
