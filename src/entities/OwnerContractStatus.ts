import {Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {OwnerContract} from "./OwnerContract";


@Entity("owner_contract_status")
export class OwnerContractStatus {

    @PrimaryGeneratedColumn({ 
        name:"id"
        })
    Id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"name"
        })
    Name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"label"
        })
    Label:string;
        

    @Column("boolean",{ 
        nullable:false,
        name:"active"
        })
    Active:boolean;
        

   
    @OneToMany(type=>OwnerContract, OwnerContract=>OwnerContract.OwnerContractStatus)
    OwnerContracts:OwnerContract[];
    
}