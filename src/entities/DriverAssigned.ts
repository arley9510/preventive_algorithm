import {Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {Vehicle} from "./Vehicle";


@Entity("driver_assigned")
export class DriverAssigned {

    @PrimaryGeneratedColumn({ 
        name:"id"
        })
    Id:number;
        

    @Column("integer",{ 
        nullable:false,
        name:"driver_id"
        })
    DriverId:number;
        

    @Column("date",{ 
        nullable:false,
        name:"date_assignment"
        })
    DateAssignment:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        default:"now()",
        name:"created_at"
        })
    CreatedAt:Date;
        

    @Column("text",{ 
        nullable:false,
        name:"observation"
        })
    Observation:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"odometer"
        })
    Odometer:number;
        

   
    @ManyToOne(type=>Vehicle, Vehicle=>Vehicle.DriverAssigneds,{  nullable:false, })
    @JoinColumn({ name:'vehicle_id'})
    Vehicle:Vehicle | null;

}
