import {Index,Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {PreventiveJob} from "./PreventiveJob";
import {PreventivePart} from "./PreventivePart";


@Entity("preventive_duration_type")
export class PreventiveDurationType {

    @PrimaryGeneratedColumn({ 
        name:"id"
        })
    Id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"name"
        })
    Name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"label"
        })
    Label:string;
        

    @Column("boolean",{ 
        nullable:false,
        name:"active"
        })
    Active:boolean;
        

   
    @OneToMany(type=>PreventiveJob, PreventiveJob=>PreventiveJob.PreventiveDurationType)
    PreventiveJobs:PreventiveJob[];
    

   
    @OneToMany(type=>PreventivePart, PreventivePart=>PreventivePart.PreventiveDurationType)
    PreventiveParts:PreventivePart[];
    
}
