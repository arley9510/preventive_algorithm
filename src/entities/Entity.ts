import { Column, Index, PrimaryGeneratedColumn, Entity } from 'typeorm';

@Entity('entity')
export class EntityTable {
    @PrimaryGeneratedColumn({
        name: 'id'
    })
    id: number;

    @Column('text', {
        nullable: true,
        name: 'gps_id'
    })
    gpsId: string;

    @Index()
    @Column('character varying', {
        nullable: false,
        length: 10,
        name: 'plate',
    })
    plate: string;

    @Column('text', {
        nullable: true,
        name: 'event_name'
    })
    eventName: string;

    @Index()
    @Column('integer', {
        nullable: false,
        name: 'gps_date'
    })
    gpsDate: number;

    @Column('integer', {
        nullable: false,
        name: 'server_date'
    })
    serverDate: number;

    @Index({ spatial: true })
    @Column({
        nullable: false,
        name: 'geom',
        type: 'geometry',
        spatialFeatureType: 'Point',
        srid: 0
    })
    Geom: string | object;

    @Column('integer', {
        nullable: false,
        name: 'speed'
    })
    speed: number;

    @Column('integer', {
        nullable: true,
        name: 'odometer'
    })
    odometer: number;

    @Column('text', {
        nullable: true,
        name: 'location'
    })
    location: string;

    @Column('integer', {
        nullable: false,
        name: 'security_id'
    })
    securityiId: number;
}
