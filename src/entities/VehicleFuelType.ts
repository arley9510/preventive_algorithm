import {Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {Vehicle} from "./Vehicle";


@Entity("vehicle_fuel_type")
export class VehicleFuelType {

    @PrimaryGeneratedColumn({ 
        name:"id"
        })
    Id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"name"
        })
    Name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"label"
        })
    Label:string;
        

    @Column("boolean",{ 
        nullable:false,
        name:"active"
        })
    Active:boolean;
        

   
    @OneToMany(type=>Vehicle, Vehicle=>Vehicle.VehicleFuelType)
    Vehicles:Vehicle[];
    
}
