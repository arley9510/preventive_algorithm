import {Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {VehicleBrand} from "./VehicleBrand";
import {Vehicle} from "./Vehicle";


@Entity("vehicle_model")
export class VehicleModel {

    @PrimaryGeneratedColumn({ 
        name:"id"
        })
    Id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"name"
        })
    Name:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:256,
        name:"label"
        })
    Label:string;
        

    @Column("boolean",{ 
        nullable:false,
        name:"active"
        })
    Active:boolean;
        

   
    @ManyToOne(type=>VehicleBrand, VehicleBrand=>VehicleBrand.VehicleModels,{  nullable:false, })
    @JoinColumn({ name:'vehicle_brand_id'})
    VehicleBrand:VehicleBrand | null;


   
    @OneToMany(type=>Vehicle, Vehicle=>Vehicle.VehicleModel)
    Vehicles:Vehicle[];
    
}
