import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { PreventivePart } from './PreventivePart';
import { PreventivePartReported } from './PreventivePartReported';
import { PreventivePartScheduled } from './PreventivePartScheduled';


@Entity('part')
export class Part {

    @PrimaryGeneratedColumn({
        name: 'id'
        })
    Id: number;


    @Column('character varying', {
        nullable: false,
        length: 256,
        name: 'name'
        })
    Name: string;


    @Column('character varying', {
        nullable: false,
        length: 256,
        name: 'label'
        })
    Label: string;


    @Column('boolean', {
        nullable: false,
        name: 'active'
        })
    Active: boolean;



    @OneToMany(type => PreventivePart, PreventivePart => PreventivePart.Part)
    PreventiveParts: PreventivePart[];

    @OneToMany(type => PreventivePartReported, PreventivePartReported => PreventivePartReported.Part)
    PreventivePartReporteds: PreventivePartReported[];

    @OneToMany(type => PreventivePartScheduled, PreventivePartScheduled => PreventivePartScheduled.Part)
    PreventivePartScheduled: PreventivePartScheduled[];

}
