import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { PreventiveJob } from './PreventiveJob';
import { PreventiveJobReported } from './PreventiveJobReported';
import { PreventiveJobScheduled } from './PreventiveJobScheduled';


@Entity('job')
export class Job {

    @PrimaryGeneratedColumn({
        name: 'id'
        })
    Id: number;


    @Column('character varying', {
        nullable: false,
        length: 256,
        name: 'name'
        })
    Name: string;


    @Column('character varying', {
        nullable: false,
        length: 256,
        name: 'label'
        })
    Label: string;


    @Column('boolean', {
        nullable: false,
        name: 'active'
        })
    Active: boolean;



    @OneToMany(type => PreventiveJob, PreventiveJob => PreventiveJob.Job)
    PreventiveJobs: PreventiveJob[];

    @OneToMany(type => PreventiveJobReported, PreventiveJobReported => PreventiveJobReported.Job)
    PreventiveJobReporteds: PreventiveJobReported[];

    @OneToMany(type => PreventiveJobScheduled, PreventiveJobScheduled => PreventiveJobScheduled.Job)
    PreventiveJobScheduled: PreventiveJobScheduled[];

}
