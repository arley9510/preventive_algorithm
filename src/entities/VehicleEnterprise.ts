import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from "typeorm";
import { GpsEnterprise } from './GpsEnterprise';
import { Vehicle } from './Vehicle';

@Entity("vehicle_enterprise")
export class VehicleEnterprise {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("integer", {
        nullable: false,
        name: "vehicle_id"
    })
    vehicle_id: number;

    @Column("integer", {
        nullable: false,
        name: "enterprise_id"
    })
    enterprise_id: number;

    @Column("integer", {
        nullable: false,
        name: "gps_enterprise"
    })
    gps_enterprise: number;

    @Column("boolean", {
        nullable: false,
        name: "active"
    })
    active: boolean;

    @ManyToOne(() => Vehicle, Vehicle => Vehicle.OwnerContracts, {nullable: false})
    @JoinColumn({name: 'vehicle_id'})
    Vehicles: Vehicle | null;

    @ManyToOne(() => GpsEnterprise, GpsEnterprise => GpsEnterprise.GpsEnterprises, {nullable: false})
    @JoinColumn({name: 'gps_enterprise'})
    VehicleEnterprise: GpsEnterprise | null;
}
