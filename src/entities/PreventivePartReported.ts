import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { Part } from './Part';
import { PreventiveJobScheduled } from './PreventiveJobScheduled';
import { PreventivePartScheduled } from './PreventivePartScheduled';


@Entity('preventive_part_reported')
export class PreventivePartReported {

    @PrimaryGeneratedColumn({
        name: 'id'
        })
    Id: number;


    @Column('date', {
        nullable: false,
        name: 'date'
        })
    Date: string;


    @Column('integer', {
        nullable: false,
        name: 'odometer'
        })
    Odometer: number;


    @Column('timestamp without time zone', {
        nullable: false,
        default: 'now()',
        name: 'created_at'
        })
    CreatedAt: Date;


    @Column('character varying', {
        nullable: false,
        length: 50,
        name: 'type'
    })
    Type: string;



    @ManyToOne(type => Part, Part => Part.PreventivePartReporteds, {  nullable: false, })
    @JoinColumn({ name: 'part_id'})
    Part: Part | null;

    @ManyToOne(type => PreventivePartScheduled, PreventivePartScheduled => PreventivePartScheduled.PreventivePartReported, {  nullable: false, })
    @JoinColumn({ name: 'preventive_part_scheduled_id'})
    PreventivePartScheduled: PreventivePartScheduled | null;

}
