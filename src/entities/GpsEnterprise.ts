import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { VehicleEnterprise } from './VehicleEnterprise';

@Entity("gps_enterprise")
export class GpsEnterprise {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("character varying", {
        nullable: false,
        length: 256,
        name: "name"
    })
    Name: string;


    @Column("character varying", {
        nullable: false,
        length: 256,
        name: "label"
    })
    Label: string;

    @OneToMany(type => VehicleEnterprise, VehicleEnterprise => VehicleEnterprise.VehicleEnterprise)
    GpsEnterprises: VehicleEnterprise[];
}
