import { Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { OwnerContractStatus } from './OwnerContractStatus';
import { Vehicle } from './Vehicle';


@Entity('owner_contract')
export class OwnerContract {

    @PrimaryGeneratedColumn({
        name: 'id'
        })
    Id: number;


    @Column('character varying', {
        nullable: false,
        length: 256,
        name: 'number'
        })
    Number: string;


    @Column('character varying', {
        nullable: true,
        length: 256,
        name: 'interest_rate'
    })
    InterestRate: string;


    @Column('date', {
        nullable: false,
        name: 'date_start'
        })
    DateStart: string;


    @Column('date', {
        nullable: false,
        name: 'date_end'
        })
    DateEnd: string;


    @Column('text', {
        nullable: true,
        name: 'observation'
        })
    Observation: string;



    @ManyToOne(type => OwnerContractStatus, OwnerContractStatus => OwnerContractStatus.OwnerContracts, {  nullable: false, })
    @JoinColumn({ name: 'owner_contract_status_id'})
    OwnerContractStatus: OwnerContractStatus | null;



    @ManyToOne(type => Vehicle, Vehicle => Vehicle.OwnerContracts, {  nullable: false, })
    @JoinColumn({ name: 'vehicle_id'})
    Vehicle: Vehicle | null;


    @Column('integer', {
        nullable: false,
        name: 'owner_id'
        })
    OwnerId: number;


    @Column('timestamp without time zone', {
        nullable: false,
        default: 'now()',
        name: 'created_at'
        })
    CreatedAt: Date;


    @Column('numeric', {
        nullable: false,
        name: 'total_payment'
        })
    TotalPayment: string;


    @Column('integer', {
        nullable: false,
        name: 'installments'
        })
    Installments: number;



}