import { ConnectionOptions } from 'typeorm';
import * as PostgresConnectionStringParser from 'pg-connection-string';

import { config } from './config';

// Get DB connection options from env variable
const connectionOptions = PostgresConnectionStringParser.parse(config.databaseUrl);

export const dbConfig: ConnectionOptions = {
    type: 'postgres',
    host: connectionOptions.host,
    port: connectionOptions.port,
    username: connectionOptions.user,
    password: connectionOptions.password,
    database: connectionOptions.database,
    dropSchema: false,
    synchronize: true,
    logging: true,
    entities: [
        'src/entities/**/*.ts'
    ],
    migrationsRun: true,
    migrations: [
        'src/migrations/**/*.ts'
    ],
    'cli': {
        'migrationsDir': 'src/migrations/**/*.ts'
    },
    extra: {
        ssl: config.dbsslconn, // if not development, will use SSL
    }
};
