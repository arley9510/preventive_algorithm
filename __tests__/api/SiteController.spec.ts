const frisby = require('frisby');

const Joi = frisby.Joi;

const url = 'http://127.0.0.1:3000/api/public/health';

describe('validate health check', () => {
    it('validate is alive', async function () {
        frisby.get(url)
            .expect('jsonTypes', {
                status: Joi.string(),
            })
            .expect('status', 200);
    });
});
