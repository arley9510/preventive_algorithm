import { BaseContext } from 'koa';
import SiteController from '../../src/controllers/public/SiteController';

test('test health check controller', async () => {
    const controller = new SiteController();

    expect(controller).toBe(controller);
});

test('test health check controller response', async () => {
    // @ts-ignore
    const  ctx: BaseContext = {
        request: {
            body: {
            },
        }
    };

    await SiteController.healthCheck(ctx).then();

    expect(ctx['body']).toEqual({status: 'OK'});
});
